Nova.booting((Vue, router, store) => {
    Vue.component('index-service-multiselect', require('./components/IndexField').default)
    Vue.component('detail-service-multiselect', require('./components/DetailField').default)
    Vue.component('form-service-multiselect', require('./components/FormField').default)
  })