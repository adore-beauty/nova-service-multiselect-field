export default {
    data() {
        return {
            options: []
        }
    },

    methods: {
        setValueFromField() {
            this.value = ''
            if (this.field.value) {
                this.value = JSON.parse(this.field.value)
            }
        },
        async setInitialOptions(contentType) {

            if (this.value.length > 0 && this.field.router ) {
                Nova.request().get(this.field.router + '/options', {
                    params: {
                        'label': this.label,
                        'value': this.field.value,
                        'contentType': contentType,

                    }
                }).then(response => {
                    this.options = response.data
                    this.value = response.data
                })
            } else if (!this.field.router) {
                console.error('please set router to retrieve options')
            }
        }
    },

    computed: {
        idColumn() {
            return this.field.modelIdColumn || 'id'
        },
        label() {
            return this.field.searchLabel || 'name'
        }
    }
}
