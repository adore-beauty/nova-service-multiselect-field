# Laravel Nova Service Multi-select Field

```php 
#import 
use Adorebeauty\ServiceMultiselect\ServiceMultiselect;

 ServiceMultiselect::make('{Field Name}', '{field_name}')
    ->optionsLabel('name') //options key name
    ->optionsIdField('id') //options value field
    ->modelIdColumn('id') // id of the vue-multiselect model
    ->setRouter('{router}') //route to while vue should send request to /api/nova-service-multiselect
    ->placeholder('{Place Holder}')
    ->dependsOn('item_matcher_type'), // the value of this field will be send to the route as content type
    ->maxOptions({$limit}),//limit per request
```