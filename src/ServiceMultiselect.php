<?php
namespace Adorebeauty\ServiceMultiselect;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class ServiceMultiselect extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'service-multiselect';

    /**
     * Set search contentType
     *
     * @param string $contentType
     * @return $this
     */
    public function optionsContentType(string $contentType)
    {
        return $this->withMeta(['contentType' => $contentType]);
    }

    /**
     * Set label for searching and frontend output
     *
     * @param string $searchLabel
     * @return $this
     */
    public function optionsLabel(string $searchLabel)
    {
        return $this->withMeta(['searchLabel' => $searchLabel]);
    }

    
    /**
     * Set id for searching and frontend output
     *
     * @param string $idField
     * @return $this
     */
    public function optionsIdField(string $idField)
    {
        return $this->withMeta(['idField' => $idField]);
    }

    /**
     * Set id for the vue multiselect form
     *
     * @param string $idField
     * @return $this
     */

    public function modelIdColumn(string $column)
    {
        return $this->withMeta(['modelIdColumn' => $column]);
    }

    /**
     * Set max Option while search which will be passed to the route
     *
     * @param integer $max
     * @return void
     */
    public function maxOptions(int $max)
    {
        return $this->withMeta(['max' => $max]);
    }

    /**
     * place holder for the multiselect
     *
     * @param string $placeholder
     * @return void
     */
    public function placeholder($placeholder)
    {
        return $this->withMeta(['placeholder' => $placeholder]);
    }

    /**
     * router to which the multiselect will query for options 
     *
     * @param string $placeholder
     * @return void
     */

    public function setRouter($router = '')
    {
        return $this->withMeta(['router' => $router]);
    }
    
    /**
     * dependeded field from which content field will be taken and pass to the given route
     *
     * @param string $placeholder
     * @return void
     */
    public function dependsOn($field)
    {
        return $this->withMeta(['dependencyField' => $field]);
    }
}